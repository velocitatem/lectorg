(setq lectorg-classes '(
                        "Discrete Math"
                        "Business Management"
                        "Fundamentals of Statistics"
                        "Learning to Observe"
                        "Calculus 0"
                        "History of Ideas"
                        )
      lectorg-classes-resources '(
                                  ("Discrete Math"
                                    "/home/velocitatem/s/Documents/College/Discrete Mathematics/Discrete Mathematics and Its Applications (Kenneth Rosen) (z-lib.org).pdf"
                                    "/home/velocitatem/s/org/20220916143409-discrete_mathematics.org"
                                    "https://blackboard.ie.edu/ultra/courses/_35436_1/outline")
                                  ("Business Management"
                                   nil
                                   "/home/velocitatem/s/org/20220914131903-introduction_to_business_management.org"
                                   "https://blackboard.ie.edu/ultra/courses/_35432_1/outline")
                                  ("Fundamentals of Statistics"
                                   "/home/velocitatem/Documents/College/Fundamentals of Probability and Statistics/Books/Probability and Statistics for Engineering and the Sciences (Jay L. Devore) (z-lib.org).pdf"
                                   "/home/velocitatem/s/org/20220919155804-fundamentals_of_probability_and_statistics.org"
                                   "https://blackboard.ie.edu/ultra/courses/_35431_1/outline")
                                  ("Learning to Observe"
                                   nil
                                   "/home/velocitatem/s/org/20220914111828-learning_to_observe_experiment_and_survey.org"
                                   "https://blackboard.ie.edu/ultra/courses/_35430_1/outline")
                                  ("Calculus 0"
                                   nil
                                   "/home/velocitatem/s/org/20220921164532-calculus_0.org"
                                   "https://blackboard.ie.edu/ultra/courses/_35428_1/outline")
                                  ("History of Ideas"
                                   nil
                                   "/home/velocitatem/s/org/20220916100103-the_big_history_of_ideas_and_innovation.org"
                                   "https://blackboard.ie.edu/ultra/courses/_35429_1/outline")
                                  )
      )
