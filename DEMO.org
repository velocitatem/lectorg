* Modeling Roastbeef
\[
	e^{0.013x}*14
\]
[[./Beef.png]]
* Some Function
\[
\frac{1}{2x}*x^{4} - 10x
\]
[[./SomeFunction.png]]


* Revolutions - CALC1 Class
#+RECORDING: 20220708:201103.wav
#+RECORDING: 20220708:201357.wav
