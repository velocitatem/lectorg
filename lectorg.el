(defcustom lectorg-backend-directory "~/Documents/Projects/lectorg/backends/" "The home directory for the backend")
(defcustom lectorg-resources '((?d "Document")
                               (?p "Presentation")
                               (?r "Research Paper")
                               (?l "Website Link")
                               (?v "Video")
                               (?a "Article")) "A list of resource types for association with notes")

(defcustom lectorg-classes '("CLASS" "CLASS") "A list of classes you are taking, for quick access to resources via the lectorg class hub")

(defcustom lectorg-classes-resources '("Class" "Book" "Notes" "Web") "A collection of resources for the lectorg class hub")


(defcustom lectorg-use-xdg-for-web nil "If t, web links will be opened using xdg")
(defcustom lectorg-command-for-web-links (if lectorg-use-xdg-for-web "xdg-open" "firefox") "If t, web links will be opened using xdg")
(defcustom lectorg-break-durations '("10:30" "11:00" "11:30" "12:00" "12:30"
                                          ;; Split for more effective break-times
                                          "16:00" "17:30" "18:20" "19:00" "19:30") "A list of durations for taking breakes developed with a piecewise linear and logorithmic function.")

(defcustom lectorg-consultation-options '() "A list of options for users to select")

(defun lectorg/consultation ()
  (interactive)
  (let ((user-selection (completing-read "How are things going? " ))))
  )


(defun lectorg/take-a-break ()
  (interactive)
  (let ((break-duration (nth (read-number "How tired are you feeling? [1-10]") lectorg-break-durations)))
    (alert (concat "Time for a " (first (split-string break-duration ":"))" break!"))
    (org-timer-set-timer (concat "00:" break-duration))
  )
)

(defun nth-elt (elt list)
  "Return element number of ELT in LIST."
  (let ((loc (length (member elt list))))
    (unless (zerop loc)
      (- (length list) loc))))

(defun lectorg/class-hub-open ()
  (interactive)
  (let ((class-index-to-use (+ 0 (nth-elt
                                  (completing-read "Select class: " lectorg-classes) lectorg-classes)))
        (class-operation (completing-read "Select action: " '("Open Book" "Open Notes" "Open Web"))))
    ;; here the program opens a file depending on which operation is selected, this is done via a lot of indexing
    (cond
     ((equal class-operation "Open Book") (progn
                                                 (shell-command (concat "xdg-open \"" (second (nth class-index-to-use lectorg-classes-resources)) "\""))
                                                 ))
     ((equal class-operation "Open Notes") (progn
                                             (org-open-file (third (nth class-index-to-use lectorg-classes-resources)))
                                            ))
     ((equal class-operation "Open Web") (progn
                                            (shell-command (concat lectorg-command-for-web-links  " \"" (fourth (nth class-index-to-use lectorg-classes-resources)) "\""))
          )
    ))
    )
  )



(defun lectorg/associate-resource ()
  (interactive)
  (let ((resource-type (second (read-multiple-choice "What kind of resource do you want to associate? " lectorg-resources ))))
        (message resource-type)
        (cond ((equal resource-type "Document") (let ((document-path (read-file-name "Select document: "))
                                                      (document-page (if (equal ?y (read-char-choice "Do you want to select a page? " '(?y ?n)))
                                                                         (read-number "Enter Page Number:") nil))  )
                                                  (progn
                                                    (if (not (equal nil document-page) )
                                                        (insert (concat "#+DOCUMENT:\n" "[[" document-path "::" document-page "]]"))
                                                      (insert (concat "#+DOCUMENT:\n" "[[" document-path "]]")))))
               )
              ((equal resource-type "Presentation")
               (let ((presentation-path (read-file-name "Select presentation: ")))
                 ;; TODO refactor these
                 (if (not (equal nil presentation-path) )
                     (insert (concat "#+PRESENTATION:\n" "[[" presentation-path "]]"))))
                 )

              ((equal resource-type "Website Link") (let ((website-path (read-string "Enter Link: ")))
                                                      (if (not (equal nil website-path))
                                                          (insert (concat "#+WEBSITE:\n" "[[" website-path "]]")))))
              )
    ))

(defun lectorg/int/motivate-me ()
  (interactive)
;;  # [2022-09-29 Thursday - 18:38]:	Find a way to integrate other packages I made
  )


(defun lectorg/complete-notes-here ()
  (interactive)
  ;; want to create a TODO and point mark
  (let ((mark-line-number (int-to-string (line-number-at-pos)))
        (mark-file-path buffer-file-name))
    (let ((mark-refference-link (concat "file:" mark-file-path "::" mark-line-number))
          (mark-title (read-string "What do you want to do here? ")))
      (org-insert-comment)
      (insert (concat "[" (format-time-string "%Y-%m-%d %A - %H:%M") "]" ":\t" mark-title))
      (f-append-text (concat "\n* TODO " "[[" mark-refference-link "][" mark-title "]]") 'utf-8 "~/.lectorg/marks.org")
      )
    )
  )

(defun lectorg/puml-chain ()
  (interactive)
  (let ((chain-list (read-string "Enter Children [comma separated]: ")))
    (insert (shell-command-to-string (concat "python " lectorg-backend-directory "double-chain.py " chain-list)))
    ))

;; not very refoactored, needs multiple let layers?
(defun lectorg/plot-function (function-name range function-latex-format)
  ;; use python backend
  (let ((file-name-slug (replace-regexp-in-string " " "" function-name)
         ))
    (let ((gnu-plot-string (format "set terminal png enhanced
set output \'%s\'
set title \"%s\"
set xrange[%s]
plot %s" (concat file-name-slug ".png") function-name range (shell-command-to-string (concat "python " lectorg-backend-directory "latex-to-symbolic.py \"" function-latex-format "\""))))
          )
      (write-region gnu-plot-string nil (concat file-name-slug ".gnuplot"))
      (list (shell-command (concat "gnuplot " (concat file-name-slug ".gnuplot")))
            (concat file-name-slug ".png"))
      )
    )
  )



(defun lectorg/generate-graph-for-function ()
  (interactive)
  (let ((function-latex-format (buffer-substring (line-beginning-position) (line-end-position)))
        (function-name (read-string "Enter function name: "))
        (function-graph-range (read-string "Enter Plot Range (min:max): "))
        )
    (let ((plot-meta (lectorg/plot-function function-name function-graph-range function-latex-format))
          (function-link-buffer-name (concat "plot-link-" function-name)))
      (if (equal (first plot-meta) 0)
          (progn
            (generate-new-buffer function-link-buffer-name)
            (switch-to-buffer function-link-buffer-name)
            (insert (concat "[[./" (second plot-meta) "]]"))
            )
        (progn
          ;; error of plot generation
          (message "Something Went Wrong")
          )))
    ))

(defun lectorg/insert-new-function ()
  (interactive)
  (let ((function-name (read-string "Enter function name: "))
        (generate-graph-p (equal 121 (read-char-choice "Generate Graph? [y/n]" '(?y ?n))))
        (function-latex-format (read-string "Enter function [latex]: ")))

    (insert (format "\\[\n\t%s \n\\]" function-latex-format))

    ;; plot comes last
    (if generate-graph-p
        (let ((function-graph-range (read-string "Enter Plot Range (min:max): ")))
          (let ((plot-meta (lectorg/plot-function function-name function-graph-range function-latex-format)))
            (if (equal (first plot-meta) 0)
                (progn
                  (insert (concat "\n[[./"(second plot-meta)"]]"))
                  )
              (progn
                ;; error of plot generation
                )))
          )
      (progn
        (message "NOT plotting function")))
    )
  )


(defun lectorg/start-recording ()
  (interactive)
  (setq shell-command-buffer-name-async "*recording shell*")
  (let ((recording-output-name (format-time-string "%Y%m%d:%H%M%S")))
    (shell-command (format "arecord %s.wav -f s16_LE &" recording-output-name))
    (insert (format "#+RECORDING: %s . wav" recording-output-name))
    (let ((process-object (first (process-list))))
      (setq lectorg-audio-recording-pid (process-id process-object)))
    )
  )


(defun lectorg/stop-recording ()
  (interactive)
  (shell-command (format "kill %s" lectorg-audio-recording-pid))
  (kill-buffer "*recording shell*")
  )
